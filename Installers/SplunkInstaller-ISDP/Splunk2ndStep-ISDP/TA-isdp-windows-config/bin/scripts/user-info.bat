<!-- : Begin batch script
@echo off
@SETLOCAL enableextensions enabledelayedexpansion
:::::::::::::::::::::::::::::::::::::::
::: CREATED BY: ISDP-SOC DEV        :::
::: DATE CREATED: 2020-10-05        :::
::: MODIFIED BY: ISDP-SOC DEV       :::
::: DATE MODIFIED: 2020-10-06       :::
:::::::::::::::::::::::::::::::::::::::

::: RUN THE EMBEDED VBScript
cscript //nologo "%~f0?.wsf" %1

exit /b

----- Begin wsf script --->
<job><script language="VBScript">

	Function NtUsr(winCmd,strToSearch,accntType)	
		If strToSearch = "Full Name" Or strToSearch = "Account active"  Or strToSearch = "Last logon"  Or strToSearch = "Password required" Or strToSearch = "Account expires" Then
			setTokens = "2,*"
			varPos = "%b"
		ElseIf strToSearch = "Password last set" Or strToSearch = "Local Group Memberships" Then
			setTokens = "2,3,*"
			varPos = "%c"
		End If
		
		Set objShell = CreateObject("WScript.Shell")
		
		If accntType = "Local" Then
			Set objExec = objShell.Exec("%comspec% /v /c (FOR /f ""tokens=" & setTokens & " delims= "" %a IN ('net user " & winCmd & " ^|findstr /B /C:""" & strToSearch & """') DO @set res=" & varPos & ")&echo !res!")
		ElseIf accntType = "Domain" Then
			Set objExec = objShell.Exec("%comspec% /v /c (FOR /f ""tokens=" & setTokens & " delims= "" %a IN ('net user " & winCmd & " /domain ^|findstr /B /C:""" & strToSearch & """') DO @set res=" & varPos & ")&echo !res!")
		End If
		
		Do
			ntUsrRaw = objExec.StdOut.ReadAll()
			ntUsrRaw = Replace(ntUsrRaw, vbCr, "")
			ntUsrRaw = Replace(ntUsrRaw, vbLf, "")
			If ntUsrRaw = "!res!" Then
				ntUsr = "N/A"
			Else
				ntUsr = ntUsrRaw
			End If
		Loop While Not objExec.Stdout.atEndOfStream
	End Function

	Function NtLocGrp(strToSearch)	
		Set objShell = CreateObject("WScript.Shell")
		comspec = objShell.ExpandEnvironmentStrings("%comspec%")
		
		Set objExec = objShell.Exec(comspec & " /c net localgroup administrators | findstr /i """ & strToSearch & """ ")
		
		Do
			ntlocgrpres = objExec.StdOut.ReadAll()

			If ntlocgrpres = "" Then
				ntlocgrp = "No"
			Else
				ntlocgrp = "Yes"
			End If
		Loop While Not objExec.Stdout.atEndOfStream
	End Function

	Function HomePath()
		Dim WshShell
		Dim objEnv
		Set WshShell = CreateObject("WScript.Shell")
		Set objEnv = WshShell.Environment("Process")
		HomePath = objEnv("HOMEPATH")
	End Function

	Function timeStamp()
		Dim t 
		t = Now
		timeStamp = Year(t) & "-" & _
		Right("0" & Month(t),2)  & "-" & _
		Right("0" & Day(t),2)  & " " & _  
		Right("0" & Hour(t),2) & ":" & _
		Right("0" & Minute(t),2) & ":" & _    
		Right("0" & Second(t),2) 
	End Function

	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colAccounts = objWMIService.ExecQuery("SELECT * FROM Win32_UserAccount WHERE LocalAccount = True")

	currentDate = timeStamp

	For Each objUser In colAccounts
		
		If strToSearch = "Full Name" Or strToSearch = "Account active"  Or strToSearch = "Last logon"  Or strToSearch = "Password expires" Then
			setTokens = "2,*"
			varPos = "%b"
		ElseIf strToSearch = "Password last set" Or strToSearch = "Local Group memberships" Then
			setTokens = "2,3,*"
			varPos = "%c"
		End If
		
		accUidRes = "N/A"
		accUserRes = Replace(objUser.Name,"|"," ")	
		accFullnameRes = Replace(NtUsr(accUserRes,"Full Name","Local"),"|"," ")	
		accRoleRes = Replace(NtLocGrp(accUserRes),"|"," ")	
		accGrpRes = Replace(NtUsr(accUserRes,"Local Group Memberships","Local"),"|"," ")	
		accActiveRes = Replace(NtUsr(accUserRes,"Account active","Local"),"|"," ")	
		accCmdShellRes = "N/A"
		accAccExpRes = Replace(NtUsr(accUserRes,"Account expires","Local"),"|"," ")	
		accLastLogRes = Replace(NtUsr(accUserRes,"Last logon","Local"),"|"," ")	
		accPassLastSetRes = Replace(NtUsr(accUserRes,"Password last set","Local"),"|"," ")	
		accAccTypeRes = "Local"
		accAPssReqRes = Replace(NtUsr(accUserRes,"Password required","Local"),"|"," ")	
		
		WScript.Echo currentDate + "|" + accUidRes + "|" + accUserRes + "|" + accFullnameRes + "|" + accRoleRes + "|" + accGrpRes + "|" + accActiveRes + "|" + accCmdShellRes + "|" + accAccExpRes + "|" + accLastLogRes + "|" + accPassLastSetRes + "|" + accAccTypeRes + "|" + accAPssReqRes
	Next


	arrPath = Split(HomePath,"\")
	pFolder = arrPath(1)

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder("C:\" & pFolder & "\")
	Set objSubFolders = objFolder.SubFolders
	 
	For each objSubFolder In objSubFolders
		arrFlUsr = split(objSubFolder,"\")
		accDomUserID = arrFlUsr(2)
		valDef = InStr(1, accDomUserID, "default", VBTextCompare) 
		valPub = InStr(1, accDomUserID, "public", VBTextCompare) 
		valAll = InStr(1, accDomUserID, "all user", VBTextCompare) 
		
		If strToSearch = "Full Name" Or strToSearch = "Account active"  Or strToSearch = "Last logon"  Or strToSearch = "Password expires" Then
			setTokens = "2,*"
			varPos = "%b"
		ElseIf strToSearch = "Password last set" Or strToSearch = "Local Group memberships" Then
			setTokens = "2,3,*"
			varPos = "%c"
		End If
		
		If valDef = 0 And valPub = 0 And valAll = 0 Then 
			
			accUserRes = Replace(accDomUserID,"|"," ")
			accFullnameRes = Replace(NtUsr(accUserRes,"Full Name","Domain"),"|"," ")
			accRoleRes = Replace(NtLocGrp(accUserRes),"|"," ")
			accGrpRes = Replace(NtUsr(accUserRes,"Local Group Memberships","Domain"),"|"," ")
			accActiveRes = Replace(NtUsr(accUserRes,"Account active","Domain"),"|"," ")
			accCmdShellRes = "N/A"
			accAccExpRes = Replace(NtUsr(accUserRes,"Account expires","Domain"),"|"," ")
			accLastLogRes = Replace(NtUsr(accUserRes,"Last logon","Domain"),"|"," ")
			accPassLastSetRes = Replace(NtUsr(accUserRes,"Password last set","Domain"),"|"," ")
			accAccTypeRes = "Domain"
			accAPssReqRes = Replace(NtUsr(accUserRes,"Password required","Domain"),"|"," ")
			
			If accFullnameRes <> "N/A"  Or accGrpRes <> "N/A" Or accActiveRes <> "N/A" Or accAccExpRes <> "N/A" Or accLastLogRes <> "N/A" Or accPassLastSetRes <> "N/A" Or accAPssReqRes <> "N/A" Then
				WScript.Echo currentDate + "|" + accUidRes + "|" + accUserRes + "|" + accFullnameRes + "|" + accRoleRes + "|" + accGrpRes + "|" + accActiveRes + "|" + accCmdShellRes + "|" + accAccExpRes + "|" + accLastLogRes + "|" + accPassLastSetRes + "|" + accAccTypeRes + "|" + accAPssReqRes
			End If
		End If
	Next
</script></job>
