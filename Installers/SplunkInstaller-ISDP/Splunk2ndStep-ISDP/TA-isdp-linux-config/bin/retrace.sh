#!/bin/bash

ifconfig_res=$(eval "ifconfig -a");
ping_res=$(eval "ping -c 5 8.8.8.8");
wget_res=$(eval "wget --server-response https://google.com 2>&1 -O /dev/null | awk '/^  HTTP/{print $2}' | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'");
trace_res=$(eval "traceroute --icmp 8.8.8.8");
echo "=============     WGET RESULT    ============="
echo "$wget_res"
echo "=============     WGET RESULT    ============="
echo $'\n'
echo "=============     PING RESULT    ============="
echo "$ping_res"
echo "=============     PING RESULT    ============="
echo $'\n'
echo "=============   IFCONFIG RESULT  ============="
echo "$ifconfig_res"
echo "=============   IFCONFIG RESULT  ============="
echo $'\n'
echo "=============     TRACEROUTE     ============="
echo "$trace_res"
echo "=============     TRACEROUTE     ============="
